
package blista.tool;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

/**
 *
 * @author mischafuerst
 * @version 0.5
 */
public class BlistaTool extends JFrame implements ActionListener{
    
    JButton         b_mount, b_unmount, b_gw_an, b_gw_aus;
    JLabel          l_mars_name, l_mars_pw, l_gateway;
    JPasswordField  pwf_mars;
    JTextField      un_mars;
    
    public BlistaTool(){
    
        super("Blista-Tool");
        setBounds(0,0,290,300);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        
        mars();
        gateway();
        
}
    
    
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == b_mount){
            Mounter mount = new Mounter();
            String user = new String(un_mars.getText());
            String pass = new String(pwf_mars.getPassword());
            mount.mounter(user, pass);
            buttonSwitch(b_mount, b_unmount);
        }
        if(e.getSource() == b_unmount){
            Unmounter unmount = new Unmounter();
            unmount.unmounter();
            buttonSwitch(b_unmount, b_mount);
        }
    }
    
    public void buttonSwitch(JButton a, JButton b){
        a.setEnabled(false);
        b.setEnabled(true);
        repaint();
    }
    
    private void mars(){
        
        b_mount = new JButton("Mount");
        b_mount.setBounds(30, 100, 100, 30);
        b_mount.setVisible(true);
        b_mount.addActionListener(this);
        
        b_unmount = new JButton("Unmount");
        b_unmount.setBounds(160, 100, 100, 30);
        b_unmount.setVisible(true);
        b_unmount.setEnabled(false);
        b_unmount.addActionListener(this);
        
        l_mars_name = new JLabel("Benutzername:");
        l_mars_name.setBounds(30, 20, 100,30);
        l_mars_name.setVisible(true);
        
        l_mars_pw = new JLabel("Passwort:");
        l_mars_pw.setBounds(30, 50, 100, 30);
        l_mars_pw.setVisible(true);
        
        un_mars = new JTextField();
        un_mars.setBounds(135, 20, 100, 30);
        un_mars.setVisible(true);
        
        pwf_mars = new JPasswordField();
        pwf_mars.setBounds(135, 50, 100, 30);
        pwf_mars.setVisible(true);
        
        
        add(b_mount);
        add(b_unmount);
        add(l_mars_name);
        add(l_mars_pw);
        add(un_mars);
        add(pwf_mars);
        repaint();
    }
    
    private void gateway(){
        
        l_gateway = new JLabel("Gateway Schalter");
        l_gateway.setBounds(30, 160, 150, 20);
        l_gateway.setVisible(true);
        
        b_gw_an = new JButton("Einschalten");
        b_gw_an.setBounds(30, 185, 100, 30);
        b_gw_an.setVisible(true);
        b_gw_an.addActionListener(this);
        
        b_gw_aus = new JButton("Ausschalten");
        b_gw_aus.setBounds(160, 185, 100, 30);
        b_gw_aus.setVisible(true);
        b_gw_aus.setEnabled(false);
        b_gw_aus.addActionListener(this);
        
        add(b_gw_aus);
        add(b_gw_an);
        add(l_gateway);
        repaint();
    }

    
    
    public static void main(String[] args) {
        BlistaTool obj = new BlistaTool();
        
    }
}

