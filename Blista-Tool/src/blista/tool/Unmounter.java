/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package blista.tool;

import java.io.IOException;

/**
 *
 * @author mischafuerst
 */
public class Unmounter extends BlistaTool{
    
    public void unmounter(){
        String[] cmd = new String[]{"osascript",
            "/Applications/Blista-Tool.app/Contents/"
                + "Resources/Java/unmounterscript" };
        
        
        try{
            Runtime rt = Runtime.getRuntime();
            Process tr = rt.exec(cmd);
            
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    
}
